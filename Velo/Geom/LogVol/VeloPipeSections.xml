<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd" >
<DDDB>
  <!-- ****************************************************** -->
  <!-- * Definition of CuBe pipe sections in velo tank      * -->
  <!-- * and supports                                       * -->
  <!-- ****************************************************** -->
  <!-- SPOKE CAP:
  - this is the small cap at the end of the spokes which are supposed
  to support the wf suppressor end and the punctured CuBe pipe.
  Let's start with a simplified conical description, not corresponding
  to actual shape.
  z_end_wf should correspond here to z_end. If not, adjust.
  - see CDD drawings:  ...
  - centered to beam axis
  - material: aluminium
  cone1:  r_in_start = 47   r_out_start = 57   z_start = -470
          r_in_end   = 27   r_out_end   = 37   z_end   = -540 -->

  <parameter name="SpokeZ" value="70*mm"/>
  <parameter name="SpokeInnerRPZ" value="47*mm"/>
  <parameter name="SpokeInnerRMZ" value="27*mm"/>
  <parameter name="SpokeOuterRPZ" value="57*mm"/>
  <parameter name="SpokeOuterRMZ" value="37*mm"/>

  <!-- COPPERBERYLLIUM PUNCTURED PIPE:
  - this is the CuBe pipe with holes inside the VELO vacuum (upstream of RF Boxes)
  - see CDD drawings:
  http://www.nikhef.nl/pub/departments/mt/projects/lhcb-vertex/pdf/TVE-LAYOUTS/TVE65.pdf
  - centered to beam axis
  - material: aluminium
  cylinder1: r_in    =   27     r_out =   39
             z_start = -540     z_end = -550
  - material: copper
  cylinder2: r_in    =   27     r_out =   27.1
             z_start = -550     z_end = -729
  - material: aluminium
  cylinder3: r_in    =   27     r_out =   39
             z_start = -729     z_end = -739
  cylinder2==cylinder3 -->

  <parameter name="PipeAlCylinderZ" value="10*mm"/>
  <parameter name="PipeAlCylinderInnerR" value="27*mm"/>
  <parameter name="PipeAlCylinderOuterR" value="39*mm"/>
  <parameter name="PipeCuCylinderZ" value="179*mm"/>
  <parameter name="PipeCuCylinderInnerR" value="27*mm"/>
  <parameter name="PipeCuCylinderOuterR" value="27.1*mm"/>
  <parameter name="PipeZ" value="PipeAlCylinderZ+PipeCuCylinderZ+PipeAlCylinderZ"/>

<!-- UPSTREAM VELO PIPE JUNCTION:
  - this is a series of pieces that connect from the CuBe punctured pipe in the
  VELO to the LHC upstream beam pipe bellow connection.
  - see CDD drawings: LHBTVV__0084, 0085, 0057 and
  http://www.nikhef.nl/pub/departments/mt/projects/lhcb-vertex/pdf/TVE-LAYOUTS/TVE65.pdf
  NB: the extra connection flange at z=-760...-800 is not shown in these drawings.
  - material: stainless steel
  cylinder1: r_in    =   27     r_out =   30
             z_start = -739     z_end = -800
  - material: stainless steel
  cylinder2: r_in    =   30     r_out =   39
             z_start = -739     z_end = -745
  - material: stainless steel
  cylinder3: r_in    =   30     r_out =   39
             z_start = -760     z_end = -800
  - material: stainless steel
  cone1:  r_in_start = 27   r_out_start = 29   z_start = -800
          r_in_end   = 38   r_out_end   = 40   z_end   = -865
  - material: stainless steel
  cylinder4: r_in    =   40     r_out =   76
             z_start = -865     z_end = -885 -->
  <parameter name="JuncSteelCyl1Z" value="61*mm"/>
  <parameter name="JuncSteelCyl1InnerR" value="27*mm"/>
  <parameter name="JuncSteelCyl1OuterR" value="30*mm"/>

  <parameter name="JuncSteelCyl2Z" value="6*mm"/>
  <parameter name="JuncSteelCyl2InnerR" value="30*mm"/>
  <parameter name="JuncSteelCyl2OuterR" value="39*mm"/>

  <parameter name="JuncSteelCyl3Z" value="40*mm"/>
  <parameter name="JuncSteelCyl3InnerR" value="30*mm"/>
  <parameter name="JuncSteelCyl3OuterR" value="39*mm"/>

<!-- Velo volume ends at -835mm so second half of the cone is moved into Before
  Magnet pipe volume. -->
  <parameter name="JuncSteelConeZ" value="35*mm"/>
  <parameter name="JuncSteelConeInnerRPZ" value="27*mm"/>
  <parameter name="JuncSteelConeInnerRMZ" value="(35*(38-27)/65)*mm+27*mm"/>
  <parameter name="JuncSteelConeOuterRPZ" value="29*mm"/>
  <parameter name="JuncSteelConeOuterRMZ" value="(35*(40-29)/65)*mm+29*mm"/>

  <parameter name="JuncSteelCyl4Z" value="20*mm"/>
  <parameter name="JuncSteelCyl4InnerR" value="40*mm"/>
  <parameter name="JuncSteelCyl4OuterR" value="76*mm"/>

  <parameter name="JuncZ" value="JuncSteelCyl1Z+JuncSteelConeZ"/>
  <parameter name="UpstreamPipeZ" value="SpokeZ+PipeZ+JuncZ"/>


  <!-- ****************************************************** -->
  <!-- * Definition of CuBe pipe sections in velo tank      * -->
  <!-- * and supports                                       * -->
  <!-- ****************************************************** -->
  <logvol name="lvVeloUpStreamPipe">
<!-- Spoke cap -->
    <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvSpokeCap" name="SpokeCap">
      <posXYZ z="-505*mm"/>
    </physvol>
<!-- CuBe pipe -->
    <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvPipeAlCylinder" name="PipeAlCyl1">
      <posXYZ z="-545*mm"/>
    </physvol>
    <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvPipeCuCylinder" name="PipeCuCyl">
      <posXYZ z="-639.5*mm"/>
    </physvol>
    <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvPipeAlCylinder" name="PipeAlCyl2">
      <posXYZ z="-734*mm"/>
    </physvol>
<!-- Pipe junction -->
    <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvPipeJuncCone1" name="PipeJuncCone1">
      <posXYZ z="-817.5*mm"/>
    </physvol>
    <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvPipeJuncCyl1" name="PipeJuncCyl1">
      <posXYZ z="-769.5*mm"/>
    </physvol>
    <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvPipeJuncCyl2" name="PipeJuncCyl2">
      <posXYZ z="-742*mm"/>
    </physvol>
    <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvPipeJuncCyl3" name="PipeJuncCyl3">
      <posXYZ z="-780*mm"/>
    </physvol>
  </logvol>

<!-- SPOKE CAP -->
    <logvol name="lvSpokeCap" material="Aluminium">
       <cons name="SpokeCapCone"
         sizeZ="SpokeZ"
         innerRadiusPZ="SpokeInnerRPZ"
         innerRadiusMZ="SpokeInnerRMZ"
         outerRadiusPZ="SpokeOuterRPZ"
         outerRadiusMZ="SpokeOuterRMZ"/>
    </logvol>

<!-- COPPERBERYLLIUM PUNCTURED PIPE -->
  <logvol name="lvPipeAlCylinder" material="Aluminium">
    <tubs name="PipeAlCylTubs"
      sizeZ="PipeAlCylinderZ"
      innerRadius="PipeAlCylinderInnerR"
      outerRadius="PipeAlCylinderOuterR"/>
  </logvol>
  <logvol name="lvPipeCuCylinder" material="Copper">
    <tubs name="PipeCuCylTubs"
      sizeZ="PipeCuCylinderZ"
      innerRadius="PipeCuCylinderInnerR"
      outerRadius="PipeCuCylinderOuterR"/>
  </logvol>

<!-- UPSTREAM VELO PIPE JUNCTION -->
  <logvol name="lvPipeJuncCyl1" material="Pipe/PipeSteel316LN">
    <tubs name="JuncSteelCyl1"
      sizeZ="JuncSteelCyl1Z"
      innerRadius="JuncSteelCyl1InnerR"
      outerRadius="JuncSteelCyl1OuterR"/>
  </logvol>
  <logvol name="lvPipeJuncCyl2" material="Pipe/PipeSteel316LN">
    <tubs name="JuncSteelCyl2"
      sizeZ="JuncSteelCyl2Z"
      innerRadius="JuncSteelCyl2InnerR+0.01*mm"
      outerRadius="JuncSteelCyl2OuterR"/>
  </logvol>
  <logvol name="lvPipeJuncCyl3" material="Pipe/PipeSteel316LN">
    <tubs name="JuncSteelCyl3"
      sizeZ="JuncSteelCyl3Z"
      innerRadius="JuncSteelCyl3InnerR+0.01*mm"
      outerRadius="JuncSteelCyl3OuterR"/>
  </logvol>
  <logvol name="lvPipeJuncCone1" material="Pipe/PipeSteel316LN">
    <cons name="JuncSteelCone"
      sizeZ="JuncSteelConeZ"
      innerRadiusPZ="JuncSteelConeInnerRPZ"
      innerRadiusMZ="JuncSteelConeInnerRMZ"
      outerRadiusPZ="JuncSteelConeOuterRPZ"
      outerRadiusMZ="JuncSteelConeOuterRMZ"/>
  </logvol>


<!-- The downstream beam pipe sections -->
<!-- NB any changes here must be propagated to the subtractions in the gas volumes included in the vacuum tank description -->

  <parameter name="ExitWindowHoleZ" value="858.6*mm"/>
  <parameter name="ExitWindowZSize" value="945*mm-ExitWindowHoleZ"/>
  <parameter name="ExitWindow2BellowsZSize" value="55.54*mm"/>
  <parameter name="BellowsStartZ" value="ExitWindowHoleZ+ExitWindow2BellowsZSize"/>
  <parameter name="BellowsInnerRadius" value="34*mm"/>
  <parameter name="BellowsOuterRadius" value="46*mm"/>
  <parameter name="BellowsThickness" value="0.3*mm"/>
  <parameter name="BellowsSpacing" value="2.7*mm"/>
  <parameter name="BellowsZSize" value="8*BellowsThickness+7*BellowsSpacing"/>
  <parameter name="DSTubeZToWeld" value="16.56*mm"/>
  <parameter name="DSTube1ZSize" value="DSTubeZToWeld+6*mm"/>
  <parameter name="DSTube1InnerRadius" value="BellowsInnerRadius"/>
  <parameter name="DSTube1Thickness" value="2.5*mm"/>
  <parameter name="DSTube2ZSize" value="2.5*mm"/>
  <parameter name="DSTube2InnerRadius" value="28*mm"/>
  <parameter name="DSTube2Thickness" value="DSTube1InnerRadius-DSTube2InnerRadius"/>
  <parameter name="DSTube3ZSize" value="12*mm"/>
  <parameter name="DSTube3InnerRadius" value="DSTube2InnerRadius"/>
  <parameter name="DSTube3Thickness" value="DSTube1Thickness"/>
  <parameter name="WFSGrooveZSize" value="2.2*mm"/>
  <parameter name="WFSGrooveRSize" value="1*mm"/>
  <parameter name="DSTube4ZSize" value="2.5*mm"/>
  <parameter name="DSTube4InnerRadius" value="27*mm"/>
  <parameter name="DSTube4Thickness" value="1*mm"/>

  <logvol name="lvVeloDownStreamPipe">
      <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvExitWindow2Bellows" name="VeloDSPipe1">
        <posXYZ z="ExitWindowHoleZ+ExitWindow2BellowsZSize/2"/>
      </physvol>
      <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvVelo2Rich1GasEW2B" name="VeloDSPipe1Gas">
        <posXYZ z="ExitWindowHoleZ+ExitWindow2BellowsZSize/2"/>
      </physvol>

      <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvBellows" name="VeloDSPipe2">
      </physvol>
      <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvVelo2Rich1GasBellows" name="VeloDSPipe2Gas">
      </physvol>

      <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvDSTubeSection" name="VeloDSPipe3">
        <posXYZ z="BellowsStartZ+BellowsZSize+(DSTube1ZSize)/2"/>
      </physvol>
      <physvol logvol="/dd/Geometry/BeforeMagnetRegion/Velo/PipeSections/lvVelo2Rich1GasDSTube" name="VeloDSPipe3Gas">
        <posXYZ z="BellowsStartZ+BellowsZSize+(DSTube1ZSize+DSTube3ZSize)/2"/>
      </physvol>
  </logvol>

  <!-- Definition of section joining Al exit window with beam pipe -->
  <logvol name="lvExitWindow2Bellows" material="Pipe/PipeAl6061">
    <cons name="ExitWindow2Bellows"
      sizeZ="ExitWindow2BellowsZSize"
      innerRadiusPZ="BellowsInnerRadius"
      innerRadiusMZ="vTankDownExitWindowHoleR-2*mm"
      outerRadiusPZ="BellowsInnerRadius+2*mm"
      outerRadiusMZ="vTankDownExitWindowHoleR"/>
  </logvol>

  <parameter name="RadiusChange" value="vTankDownExitWindowHoleR-(BellowsInnerRadius+2*mm)"/>

  <logvol name="lvVelo2Rich1GasEW2B" material="RichMaterials/C4F10">
    <subtraction name="V2R1GasEW2BSub">
      <tubs name="V2R1GasEW2BTubs1"
          sizeZ="ExitWindow2BellowsZSize"
          outerRadius="vTankDownExitWindowHoleR"/>
      <cons name="V2R1GasEW2BCons1"
          sizeZ="2*ExitWindow2BellowsZSize"
          innerRadiusPZ="0*mm"
          innerRadiusMZ="0*mm"
          outerRadiusPZ="(BellowsInnerRadius+2*mm)-RadiusChange/2"
          outerRadiusMZ="vTankDownExitWindowHoleR+RadiusChange/2"/>
    </subtraction>
  </logvol>

  <!-- Definition of the bellows section of the beam pipe -->
  <logvol name="lvBellows" material="Pipe/PipeAl6061">
    <polycone name="Bellows">
      <!-- 1 -->  <zplane z="BellowsStartZ"                                             innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 2 -->  <zplane z="BellowsStartZ+BellowsThickness"                            innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 3 -->  <zplane z="BellowsStartZ+BellowsThickness+Epsilon"                    innerRadius="BellowsOuterRadius-BellowsThickness" outerRadius="BellowsOuterRadius" />
      <!-- 4 -->  <zplane z="BellowsStartZ+BellowsThickness+BellowsSpacing"             innerRadius="BellowsOuterRadius-BellowsThickness" outerRadius="BellowsOuterRadius" />
      <!-- 5 -->  <zplane z="BellowsStartZ+BellowsThickness+BellowsSpacing+Epsilon"     innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 6 -->  <zplane z="BellowsStartZ+2*BellowsThickness+BellowsSpacing"           innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 7 -->  <zplane z="BellowsStartZ+2*BellowsThickness+BellowsSpacing+Epsilon"   innerRadius="BellowsInnerRadius"                  outerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 8 -->  <zplane z="BellowsStartZ+2*BellowsThickness+2*BellowsSpacing"         innerRadius="BellowsInnerRadius"                  outerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 9 -->  <zplane z="BellowsStartZ+2*BellowsThickness+2*BellowsSpacing+Epsilon" innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 10 --> <zplane z="BellowsStartZ+3*BellowsThickness+2*BellowsSpacing"         innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 11 --> <zplane z="BellowsStartZ+3*BellowsThickness+2*BellowsSpacing+Epsilon" innerRadius="BellowsOuterRadius-BellowsThickness" outerRadius="BellowsOuterRadius" />
      <!-- 12 --> <zplane z="BellowsStartZ+3*BellowsThickness+3*BellowsSpacing"         innerRadius="BellowsOuterRadius-BellowsThickness" outerRadius="BellowsOuterRadius" />
      <!-- 13 --> <zplane z="BellowsStartZ+3*BellowsThickness+3*BellowsSpacing+Epsilon" innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 14 --> <zplane z="BellowsStartZ+4*BellowsThickness+3*BellowsSpacing"         innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 15 --> <zplane z="BellowsStartZ+4*BellowsThickness+3*BellowsSpacing+Epsilon" innerRadius="BellowsInnerRadius"                  outerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 16 --> <zplane z="BellowsStartZ+4*BellowsThickness+4*BellowsSpacing"         innerRadius="BellowsInnerRadius"                  outerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 17 --> <zplane z="BellowsStartZ+4*BellowsThickness+4*BellowsSpacing+Epsilon" innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 18 --> <zplane z="BellowsStartZ+5*BellowsThickness+4*BellowsSpacing"         innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 19 --> <zplane z="BellowsStartZ+5*BellowsThickness+4*BellowsSpacing+Epsilon" innerRadius="BellowsOuterRadius-BellowsThickness" outerRadius="BellowsOuterRadius" />
      <!-- 20 --> <zplane z="BellowsStartZ+5*BellowsThickness+5*BellowsSpacing"         innerRadius="BellowsOuterRadius-BellowsThickness" outerRadius="BellowsOuterRadius" />
      <!-- 21 --> <zplane z="BellowsStartZ+5*BellowsThickness+5*BellowsSpacing+Epsilon" innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 22 --> <zplane z="BellowsStartZ+6*BellowsThickness+5*BellowsSpacing"         innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 23 --> <zplane z="BellowsStartZ+6*BellowsThickness+5*BellowsSpacing+Epsilon" innerRadius="BellowsInnerRadius"                  outerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 24 --> <zplane z="BellowsStartZ+6*BellowsThickness+6*BellowsSpacing"         innerRadius="BellowsInnerRadius"                  outerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 25 --> <zplane z="BellowsStartZ+6*BellowsThickness+6*BellowsSpacing+Epsilon" innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 26 --> <zplane z="BellowsStartZ+7*BellowsThickness+6*BellowsSpacing"         innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 27 --> <zplane z="BellowsStartZ+7*BellowsThickness+6*BellowsSpacing+Epsilon" innerRadius="BellowsOuterRadius-BellowsThickness" outerRadius="BellowsOuterRadius" />
      <!-- 28 --> <zplane z="BellowsStartZ+7*BellowsThickness+7*BellowsSpacing"         innerRadius="BellowsOuterRadius-BellowsThickness" outerRadius="BellowsOuterRadius" />
      <!-- 29 --> <zplane z="BellowsStartZ+7*BellowsThickness+7*BellowsSpacing+Epsilon" innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
      <!-- 30 --> <zplane z="BellowsStartZ+8*BellowsThickness+7*BellowsSpacing"         innerRadius="BellowsInnerRadius"                  outerRadius="BellowsOuterRadius" />
    </polycone>
  </logvol>

<!-- The RICH1 gas sections DS of the exit window -->

    <logvol name="lvVelo2Rich1GasBellows" material="RichMaterials/C4F10">
      <polycone name="V2R1GasBellows">
      <!-- 1 -->  <zplane z="BellowsStartZ"                                             outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsOuterRadius" />
      <!-- 6 -->  <zplane z="BellowsStartZ+2*BellowsThickness+BellowsSpacing"           outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsOuterRadius" />
      <!-- 7 -->  <zplane z="BellowsStartZ+2*BellowsThickness+BellowsSpacing+Epsilon"   outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 8 -->  <zplane z="BellowsStartZ+2*BellowsThickness+2*BellowsSpacing"         outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 9 -->  <zplane z="BellowsStartZ+2*BellowsThickness+2*BellowsSpacing+Epsilon" outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsOuterRadius" />
      <!-- 14 --> <zplane z="BellowsStartZ+4*BellowsThickness+3*BellowsSpacing"         outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsOuterRadius" />
      <!-- 15 --> <zplane z="BellowsStartZ+4*BellowsThickness+3*BellowsSpacing+Epsilon" outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 16 --> <zplane z="BellowsStartZ+4*BellowsThickness+4*BellowsSpacing"         outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 17 --> <zplane z="BellowsStartZ+4*BellowsThickness+4*BellowsSpacing+Epsilon" outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsOuterRadius" />
      <!-- 22 --> <zplane z="BellowsStartZ+6*BellowsThickness+5*BellowsSpacing"         outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsOuterRadius" />
      <!-- 23 --> <zplane z="BellowsStartZ+6*BellowsThickness+5*BellowsSpacing+Epsilon" outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 24 --> <zplane z="BellowsStartZ+6*BellowsThickness+6*BellowsSpacing"         outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsInnerRadius+BellowsThickness" />
      <!-- 25 --> <zplane z="BellowsStartZ+6*BellowsThickness+6*BellowsSpacing+Epsilon" outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsOuterRadius" />
      <!-- 30 --> <zplane z="BellowsStartZ+8*BellowsThickness+7*BellowsSpacing-Epsilon" outerRadius="vTankDownExitWindowHoleR" innerRadius="BellowsOuterRadius" />
      </polycone>
    </logvol>

  <!-- Definition of the tube section of the beam pipe -->
  <!-- TODO: Looks like the groove for the connection of the wakefield suppressor is in the wrong place?
             It's DS edge is currently located at 968mm but the wakefield suppressor ends at 954mm -->
  <logvol name="lvDSTubeSection" material="Pipe/PipeAl2219F">
    <union name="DSTubeUnion">
      <tubs name="DSTube1"
        sizeZ="DSTube1ZSize"
        innerRadius="DSTube1InnerRadius"
        outerRadius="DSTube1InnerRadius+DSTube1Thickness"/>
      <tubs name="DSTube2"
        sizeZ="DSTube2ZSize"
        innerRadius="DSTube2InnerRadius"
        outerRadius="DSTube2InnerRadius+DSTube2Thickness"/>
      <posXYZ z="(DSTube1ZSize-DSTube2ZSize)/2"/>
      <subtraction name="DSTube3Sub">
        <tubs name="DSTube3a"
          sizeZ="DSTube3ZSize"
          innerRadius="DSTube3InnerRadius"
          outerRadius="DSTube3InnerRadius+DSTube3Thickness"/>
        <tubs name="DSTube3b"
	  sizeZ="WFSGrooveZSize"
	  innerRadius="0*mm"
	  outerRadius="DSTube3InnerRadius+WFSGrooveRSize"/>
	<posXYZ z="(DSTube3ZSize-WFSGrooveZSize)/2-2*mm"/>
      </subtraction>
      <posXYZ z="(DSTube1ZSize+DSTube3ZSize)/2"/>
    </union>
  </logvol>

  <logvol name="lvVelo2Rich1GasDSTube" material="RichMaterials/C4F10">
    <subtraction name="DSTubeGasSub">
      <tubs name="DSTubeGas"
        sizeZ="DSTube1ZSize+DSTube3ZSize"
        innerRadius="0*mm"
        outerRadius="vTankDownExitWindowHoleR"/>
      <tubs name="DSTube1Gas"
        sizeZ="2*DSTube1ZSize"
        innerRadius="0*mm"
        outerRadius="DSTube1InnerRadius+DSTube1Thickness"/>
      <posXYZ z="-(DSTube1ZSize+DSTube3ZSize)/2"/>
      <tubs name="DSTube3Gas"
        sizeZ="2*DSTube3ZSize"
        innerRadius="0*mm"
        outerRadius="DSTube3InnerRadius+DSTube3Thickness"/>
      <posXYZ z="(DSTube1ZSize+DSTube3ZSize)/2"/>
    </subtraction>
  </logvol>

</DDDB>
