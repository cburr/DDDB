<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd"[
<!ENTITY BlsParams SYSTEM "git:/Bls/parameters.xml">
]>
<DDDB>

<!-- ***************************************************************** -->
<!-- * Beam Loss Scintillator                                        * -->
<!-- * Author: Vadim Talanov                                         * -->
<!-- * Description: the BLS system upstream of Velo                  * -->
<!-- ***************************************************************** -->

  &BlsParams;

  <catalog name = "Bls">
   <logvolref href = "#lvBlsScintS" />
   <logvolref href = "#lvBlsScintM" />
   <logvolref href = "#lvBlsScintL" />
   <logvolref href = "#lvBlsFiber" />
   <logvolref href = "#lvBlsRing" />
   <logvolref href = "#lvBlsPmt" />
   <logvolref href = "#lvBlsFill" />
   <logvolref href = "#lvBlsTube" />
   <logvolref href = "#lvBls34" />
   <logvolref href = "#lvBls56" />
   <logvolref href = "#lvBls78" />
  </catalog>

  <logvol name = "lvBlsScintS"
	  material = "Bls/BlsSc"
	  sensdet = "GiGaSensDetTracker/BlsSDet" >
    <box name = "BlsScintBoxS"
	 sizeX = "BlsScintSSizeX"
	 sizeY = "BlsScintSSizeY"
	 sizeZ = "BlsScintSSizeZ" />
  </logvol>

  <logvol name = "lvBlsScintM"
	  material = "Bls/BlsSc"
	  sensdet = "GiGaSensDetTracker/BlsSDet" >
    <box name = "BlsScintBoxM"
	 sizeX = "BlsScintMSizeX"
	 sizeY = "BlsScintMSizeY"
	 sizeZ = "BlsScintMSizeZ" />
  </logvol>

  <logvol name = "lvBlsScintL"
	  material = "Bls/BlsSc"
	  sensdet = "GiGaSensDetTracker/BlsSDet" >
    <box name = "BlsScintBoxL"
	 sizeX = "BlsScintLSizeX"
	 sizeY = "BlsScintLSizeY"
	 sizeZ = "BlsScintLSizeZ" />
  </logvol>

  <logvol name = "lvBlsFiber"
	  material = "Bls/BlsResin" >
    <trd name = "BlsFiber"
         sizeZ  = "BlsFiberCoverH"
         sizeX1 = "BlsScintLSizeX"
         sizeY1 = "BlsScintSSizeZ"
         sizeX2 = "BlsScintSSizeX"
         sizeY2 = "BlsScintSSizeZ" />
  </logvol>

  <logvol name = "lvBlsRing"
	  material = "Bls/BlsVinyp" >
    <tubs name = "BlsRingTub"
          sizeZ = "BlsRingH"
          innerRadius = "BlsRingInnerD / 2.0 + myGeometryPrecision"
          outerRadius = "BlsRingOuterD / 2.0" />
  </logvol>

  <logvol name = "lvBlsPmt"
	  material = "Bls/BlsAlu" >
    <tubs name = "BlsPmtTub"
          sizeZ = "BlsPmtH"
	  innerRadius = "BlsPmtInnerD / 2.0"
          outerRadius = "BlsPmtOuterD / 2.0" />
  </logvol>

  <logvol name = "lvBlsFill"
	  material = "Bls/BlsResin" >
    <tubs name = "BlsFillTub"
          sizeZ = "BlsFillH"
          outerRadius = "BlsFillOuterD / 2.0" />
  </logvol>

  <logvol name = "lvBlsTube"
	  material = "Bls/BlsSteel" >
    <tubs name = "BlsTubeTub"
          sizeZ = "BlsTubeH"
          innerRadius = "BlsTubeInnerD / 2.0 + myGeometryPrecision"
          outerRadius = "BlsTubeOuterD / 2.0" />
  </logvol>

<!-- BLS3/4 two inner small -->

  <logvol name = "lvBls34" >

    <physvol name = "pvBlsFiber34"
       logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsFiber" >
      <posXYZ y = "+1.0 *
	      (BlsScintSSizeY / 2.0 + BlsFiberCoverH / 2.0)" />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsScint34M"
       logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsScintM" >
      <posXYZ y = "+1.0 *
	      (BlsScintSSizeY / 2.0 + BlsFiberCoverH + BlsScintMSizeY / 2.0) " />
    </physvol>

    <physvol name = "pvBlsRing34"
       logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsRing" >
      <posXYZ y = "+0.0*mm" />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsPmt34"
       logvol="/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsPmt" >
      <posXYZ y = "-1.0 *
	      (BlsScintSSizeY / 2.0 + BlsPmtH / 2.0) " />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsFill34"
       logvol="/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsFill" >
      <posXYZ y = "-1.0 *
	      (BlsScintSSizeY / 2.0 + BlsPmtH + BlsFillH / 2.0) " />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsTube34"
        logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsTube" >
      <posXYZ y = "-1.0 *
	      (BlsTubeH / 2.0 - BlsScintSSizeY / 2.0)" />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

  </logvol>

<!-- BLS5/6 two outer large -->

  <logvol name = "lvBls56" >

    <physvol name = "pvBlsFiber56"
       logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsFiber" >
      <posXYZ y = "+1.0 *
	      (BlsScintSSizeY / 2.0 + BlsFiberCoverH / 2.0)" />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsScint56L"
       logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsScintL" >
      <posXYZ y = "+1.0 *
	      (BlsScintSSizeY / 2.0 + BlsFiberCoverH + BlsScintLSizeY / 2.0) " />
    </physvol>

    <physvol name = "pvBlsRing56"
       logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsRing" >
      <posXYZ y = "+0.0*mm" />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsPmt56"
       logvol="/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsPmt" >
      <posXYZ y = "-1.0 *
	      (BlsScintSSizeY / 2.0 + BlsPmtH / 2.0) " />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsFill56"
       logvol="/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsFill" >
      <posXYZ y = "-1.0 *
	      (BlsScintSSizeY / 2.0 + BlsPmtH + BlsFillH / 2.0) " />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsTube56"
        logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsTube" >
      <posXYZ y = "-1.0 *
	      (BlsTubeH / 2.0 - BlsScintSSizeY / 2.0)" />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

  </logvol>

<!-- BLS7/8 two old standard -->

  <logvol name = "lvBls78" >

    <physvol name = "pvBlsScint78"
       logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsScintS" />

    <physvol name = "pvBlsRing78"
       logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsRing" >
      <posXYZ y = "+0.0*mm" />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsPmt78"
       logvol="/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsPmt" >
      <posXYZ y = "-1.0 *
	      (BlsScintSSizeY / 2.0 + BlsPmtH / 2.0) " />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsFill78"
       logvol="/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsFill" >
      <posXYZ y = "-1.0 *
	      (BlsScintSSizeY / 2.0 + BlsPmtH + BlsFillH / 2.0) " />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

    <physvol name = "pvBlsTube78"
        logvol = "/dd/Geometry/BeforeMagnetRegion/BeforeVelo/Bls/lvBlsTube" >
      <posXYZ y = "-1.0 *
	      (BlsTubeH / 2.0 - BlsScintSSizeY / 2.0)" />
      <rotXYZ rotX = "90.0*degree" />
    </physvol>

  </logvol>

</DDDB>
