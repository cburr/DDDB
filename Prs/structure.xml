<?xml version="1.0" encoding="ISO-8859-1"?>

<!DOCTYPE DDDB SYSTEM "git:/DTD/structure.dtd"[
<!ENTITY GeomParameters SYSTEM "git:/Prs/GeomParam.xml">
<!ENTITY DetElemParameters SYSTEM "git:/Prs/DetElemParam.xml">
]>


<DDDB>

<!--
  **************************************************************************
  *                                                                        *
  *  Version 2.0 of the first detailed Prs description by Grigori Rybkine  *
  *                         Grigori.Rybkine@cern.ch                        *
  *                                                                        *
  **************************************************************************
-->
  &GeomParameters;

  <!-- ########### Prs effective cell sizes ########### -->
  <parameter name="InCell"      value="PrsModXYSize/12" />
  <parameter name="MidCell"     value="PrsModXYSize/8"  />
  <parameter name="OutCell"     value="PrsModXYSize/4"  />
  <parameter name="OutID"       value="0"  />
  <parameter name="MidID"       value="1"  />
  <parameter name="InID"        value="2"  />
  <parameter name="ASide"       value="1"  />
  <parameter name="CSide"       value="-1"  />

  <!-- ***************************************************************** -->
  <!--                  Define detector elements for Prs                 -->
  <!-- ***************************************************************** -->

  <detelem name = "Prs" classID = "8900">
    <author> Grigori Rybkine Grigori.Rybkine@cern.ch </author>
    <version>             3.0                        </version>
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Prs/Installation/Prs"
                  condition = "/dd/Conditions/Alignment/Prs/PrsSystem"
                  npath   = "PrsSubsystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion"/>

    <conditioninfo name      = "Hardware"
                   condition = "/dd/Conditions/ReadoutConf/Prs/Hardware" />

    <conditioninfo name      = "Readout"
                   condition = "/dd/Conditions/ReadoutConf/Prs/Readout" />

    <conditioninfo name      = "Gain"
                   condition = "/dd/Conditions/Calibration/Prs/Gain" />

    <conditioninfo name      = "Reco"
                   condition = "/dd/Conditions/Calibration/Prs/Reco" />

    <conditioninfo name      = "Calibration"
                   condition = "/dd/Conditions/Calibration/Prs/Calibration" />

    <conditioninfo name      = "Quality"
                   condition = "/dd/Conditions/Calibration/Prs/Quality" />

    <conditioninfo name      = "LEDReference"
                          condition = "/dd/Conditions/Calibration/Prs/LEDReference" />

    <conditioninfo name      = "NumericGains"
                          condition = "/dd/Conditions/Calibration/Prs/NumericGains" />

    &DetElemParameters;

    <detelemref href = "#PrsC"   />
    <detelemref href = "#PrsA"   />


  </detelem>

  <!-- ***************************************************************** -->

  <detelem name = "PrsA"    classID="8901" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Prs/Installation/PrsLeft"
                  condition = "/dd/Conditions/Alignment/Prs/PrsASystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Prs"
                  npath   = "PrsA"/>

    <userParameter name="CaloSide" type="int"> ASide </userParameter>

    <detelemref href = "#PrsAOuter"   />
    <detelemref href = "#PrsAMiddle"  />
    <detelemref href = "#PrsAInner"   />
  </detelem>

  <detelem name = "PrsC"    classID="8901" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Prs/Installation/PrsRight"
                  condition = "/dd/Conditions/Alignment/Prs/PrsCSystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Prs"
                  npath   = "PrsC"/>

    <userParameter name="CaloSide" type="int"> CSide </userParameter>

    <detelemref href = "#PrsCOuter"   />
    <detelemref href = "#PrsCMiddle"   />
    <detelemref href = "#PrsCInner"   />

  </detelem>

  <!-- ***************************************************************** -->

  <detelem name = "PrsAInner"    classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Prs/Installation/InnerSectionLeft"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Prs/PrsA"
                  npath   = "PrsAInner"/>

    <userParameter name="CellSize" type="double"> InCell </userParameter>
    <userParameter name="Area"     type="int"> InID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*PrsInnXSize</userParameter>
    <userParameter name="YSize" type="double"> PrsInnYSize </userParameter>

  </detelem>

  <detelem name = "PrsAMiddle"  classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Prs/Installation/MiddleSectionLeft"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Prs/PrsA"
                  npath   = "PrsAMiddle"/>

    <userParameter name="CellSize" type="double"> MidCell </userParameter>
    <userParameter name="Area"     type="int"> MidID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*PrsMidXSize</userParameter>
    <userParameter name="YSize" type="double"> PrsMidYSize </userParameter>
  </detelem>

  <detelem name = "PrsAOuter"   classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Prs/Installation/OuterSectionLeft"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Prs/PrsA"
                  npath   = "PrsAOuter"/>

    <userParameter name="CellSize" type="double"> OutCell </userParameter>
    <userParameter name="Area"     type="int"> OutID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*PrsOutXSize</userParameter>
    <userParameter name="YSize" type="double"> PrsOutYSize </userParameter>

  </detelem>

  <detelem name = "PrsCInner"    classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Prs/Installation/InnerSectionRight"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Prs/PrsC"
                  npath   = "PrsCInner"/>

    <userParameter name="CellSize" type="double"> InCell </userParameter>
    <userParameter name="Area"     type="int"> InID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*PrsInnXSize</userParameter>
    <userParameter name="YSize" type="double"> PrsInnYSize </userParameter>
  </detelem>


  <detelem name = "PrsCMiddle"  classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Prs/Installation/MiddleSectionRight"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Prs/PrsC"
                  npath   = "PrsCMiddle"/>

    <userParameter name="CellSize" type="double"> MidCell </userParameter>
    <userParameter name="Area"     type="int"> MidID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*PrsMidXSize</userParameter>
    <userParameter name="YSize" type="double"> PrsMidYSize </userParameter>

  </detelem>

  <detelem name = "PrsCOuter"   classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Prs/Installation/OuterSectionRight"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Prs/PrsC"
                  npath   = "PrsCOuter"/>

    <userParameter name="CellSize" type="double"> OutCell </userParameter>
    <userParameter name="Area"     type="int"> OutID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*PrsOutXSize</userParameter>
    <userParameter name="YSize" type="double"> PrsOutYSize </userParameter>
  </detelem>




<!-- ***************************************************************** -->
  <detelem name = "Converter" type = "passive">
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Prs/Converter"
                  npath   = "Converter"
                  support = "/dd/Structure/LHCb/DownstreamRegion"/>

  </detelem>
<!-- ***************************************************************** -->
</DDDB>
