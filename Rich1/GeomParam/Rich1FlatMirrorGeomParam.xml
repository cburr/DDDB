<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- This file has the geometry parameters for Rich1 Mirror  -->
<!--   Begin   Rich1MirrorGeomParameters     -->
<!--  This is for the Vertical Rich1.            -->
<!-- Mirror2: There is four per each quadrant. The Cherenkov photons normally
     get reflected first from Mirror1 and then from Mirror2.
     The mirror2 is a set of spherical segments with
     large radii of curvature.
     In the default arrangement they are kept in such way that all the segments have the
     same orientation in LHCb. This means that the lines from the center of each segment
     the corresponding Center of curvature are parallel lines.  August 2005.
     Each mirror is set as a separate logical volume so that it can has its own radius
     of curvature. The quadrant numbering scheme is changed in March 2008 to match that
     of the primary mirrors of Rich1.

   -->

<!--   The mirror quadrants are named   0 1  when looking along  -->
<!--                                    3 2  the positive Z axis -->
<!--  Hence all the 4 mirror sectors        Q0             Q1
      are when looking downstream
      along positive Z axis                       beam

                                            Q3             Q2

       The Mirror2 copy numbers for the G4 physical volume are as follows.
       Looking downstream from the interaction point along positive Z axis:

            C0   C1                 C2      C3

     R0     0      1                 4       5    R0
               Q0                       Q1             H0   (top)
     R1     2      3                 6       7    R1

             A side        beam     C side

    R2      8     9                 12      13    R2
               Q3                       Q2             H1 (bottom)
    R3      10    11                14      15    R3




     These correspond to the hardware mirror numbering in the edms notes
      857711 and 857321 as follows.

       Software      Hardware
        0             21
        1             19
        2             23
        3             24

        4             17
        5             14
        6             18
        7             15


        8             06
        9             05
       10             01
       11             02


       12             12
       13             09
       14             13
       15             07



      The Mirror1 is a set of spherical segments and Mirror2 is also a set of spherical segments.
      There are 16 logical volumes for mirror2.
      There a logical volume for the mirror2 support panel which is a box. It is placed in
      two physical volumes, one above the beam pipe and one below the beam pipe.
      The copy numbers of the mirror2 support panels are

        H0 (top)   0

        beam

        H1 (bottom) 1

      The Top mirror2 segments are inside a master volume made of c4f10 in the shape of a box.
      The bottom  mirror2 segments are inside a master volume made of c4f10 in the shape of a box.
      Survey measurments as used in EDMS document 860274 and 857711 and message from D.Websdale on
      September 21-2007 and info from F. Metlica are used as input.
      Modified in August 2008 after the survey by Dave Websdale.
      SE.
 -->

   <parameter name="Rh1Mirror2SingleXSize" value="347.5*mm"   />
   <parameter name="Rh1Mirror2SingleYSize" value="380.5*mm"   />
   <parameter name="Rh1Mirror2ZSize" value="6.0*mm"   />
<!-- The signs for the following four quantities are assigned when defining the
    Rich1Mirror2Master Rotations
    The following modified after the survey by DW in August 2008.
    They are further modified in Dec 2009 after new survey by DW .
    The signs for the TopVert and TopHoriz are flipped few lines later,
    to get the correct sign conventions.

   Values until Aug 2008.

   <parameter name="Rh1Mirror2TopVertTilt" value="0.2498*rad" />
   <parameter name="Rh1Mirror2BotVertTilt" value="0.2506*rad" />
   <parameter name="Rh1Mirror2TopHorizTilt" value="0.0006*rad" />
   <parameter name="Rh1Mirror2BotHorizTilt" value="0.0003*rad" />

  values until Dec 2009
   <parameter name="Rh1Mirror2TopVertTilt" value="0.2501*rad" />
   <parameter name="Rh1Mirror2BotVertTilt" value="0.2505*rad" />
   <parameter name="Rh1Mirror2TopHorizTilt" value="0.0005*rad" />
   <parameter name="Rh1Mirror2BotHorizTilt" value="0.0004*rad" />

  Now the values used from Dec-10-2009.
-->
   <parameter name="Rh1Mirror2TopVertTilt" value="0.2502*rad" />
   <parameter name="Rh1Mirror2BotVertTilt" value="0.2506*rad" />
   <parameter name="Rh1Mirror2TopHorizTilt" value="0.0004*rad" />
   <parameter name="Rh1Mirror2BotHorizTilt" value="0.0006*rad" />

   <!-- The following are nominal values -->
    <parameter name="Rh1Mirror2VertTilt"  value="0.2502*rad"  />
    <parameter name="Rh1Mirror2InnerR"  value="1000.0*m"  />
   <!-- End of nominal values -->

   <!-- Gap between adjacent  mirror2 segments -->
    <parameter name="Rh1Mirror2XNetGap"  value="3.0*mm"    />
    <parameter name="Rh1Mirror2YNetGap"  value="3.0*mm"    />
   <!-- Gap between the mirror and the Rh1mirro2master on the downstream side and bottom edge -->
    <parameter name="Rh1Mirror2ToMasterDnsSurfGap" value="4.0*mm" />
    <parameter name="Rh1Mirror2ToMasterLocalYGap" value="6.0*mm" />

   <!-- Y and Z of bottom edge of reflecting surface wrt the LHCborigin.
     This now refers to a plane which is tangential to the reflecting surface of the mirror.
     These are from the message from D.Websdale on 21-9-2007 as a result of survey.
     The following modified after a second survey in August 2008 by D.Websdale.
     They are further modified after new survey in Dec-2009
     values until Aug-2008
    <parameter name="Rh1Mirror2BotCornerLHCbY" value="350.0*mm"  />
    <parameter name="Rh1Mirror2BotCornerLHCbZ" value="1335.0*mm"  />
     values until Dec-2009
    <parameter name="Rh1Mirror2BotCornerLHCbY" value="350.0*mm"  />
    <parameter name="Rh1Mirror2BotCornerLHCbZ" value="1332.2*mm"  />
    Now for values used from Dec-10-2009. From this date the top and
    bottom positions are not symmetric wrt the beam axis.
   -->



   <parameter name="Rh1Mirror2BotCornerOfTopMirrLHCbY" value="349.0*mm"  />
   <parameter name="Rh1Mirror2BotCornerOfTopMirrLHCbZ" value="1333.7*mm"  />
   <parameter name="Rh1Mirror2TopCornerOfBotMirrLHCbY" value="-351.3*mm"  />
   <parameter name="Rh1Mirror2TopCornerOfBotMirrLHCbZ" value="1335.4*mm"  />

     <!-- The following two lines can be removed in the future -->
    <parameter name="Rh1Mirror2BotCornerLHCbY" value="349.0*mm"  />  <!-- Kept for backward compatibility with TopMirrValue -->
    <parameter name="Rh1Mirror2BotCornerLHCbZ" value="1333.7*mm"  /> <!-- Kept for backward compatibility with TopMirrValue -->


   <parameter name = "Rh1NumberOfMirror2Segments"  value="16" />
   <parameter name = "Rh1NumberOfMirror2SegmentsInaHalf" value="8" />
   <parameter name= "Rh1NumMirror2SegmentsInaQuadrant" value="4" />
   <parameter name= "Rh1NumMirror2ColInAQuad" value="2" />
   <parameter name= "Rh1NumMirror2RowInAQuad" value="2" />
   <parameter name="Rh1Mirror2NumRows" value="2" />
   <parameter name="Rh1Mirror2NumColumns" value="4" />


<!-- Now for the Rich1Mirror2Master size orientation and location -->



  <parameter name="Rh1Mirror2MasterXSize" value="1500.0*mm" />
  <parameter name="Rh1Mirror2MasterYSize" value="780.0*mm" />
  <parameter name="Rh1Mirror2MasterZSize" value="14.0*mm" />


  <parameter name="Rh1Mirror2MasterShiftInLocalZ" value="Rh1Mirror2ToMasterDnsSurfGap-0.5*Rh1Mirror2MasterZSize" />
  <parameter name="Rh1Mirror2MasterShiftInLocalY" value="-Rh1Mirror2ToMasterLocalYGap + 0.5*Rh1Mirror2MasterYSize" />


  <parameter name="Rh1Mirror2MasterTopVertTilt"   value="Rh1Mirror2TopVertTilt" />
  <parameter name="Rh1Mirror2MasterBotVertTilt"   value="Rh1Mirror2BotVertTilt" />
  <parameter name="Rh1Mirror2MasterTopXRot"      value="-1.0*Rh1Mirror2MasterTopVertTilt" />
  <parameter name="Rh1Mirror2MasterBotXRot"      value="Rh1Mirror2MasterBotVertTilt" />
  <parameter name="Rh1Mirror2MasterTopYRot"      value="-1.0*Rh1Mirror2TopHorizTilt" />
  <parameter name="Rh1Mirror2MasterBotYRot"      value="Rh1Mirror2BotHorizTilt" />


  <parameter name="Rh1Mirror2MasterTopXLocationInLHCb" value="0.0*mm" />
  <parameter name="Rh1Mirror2MasterTopYLocationInLHCb" value="Rh1Mirror2BotCornerOfTopMirrLHCbY + Rh1Mirror2MasterShiftInLocalY*cos(-1.0*Rh1Mirror2MasterTopXRot)+ Rh1Mirror2MasterShiftInLocalZ*sin(-1.0*Rh1Mirror2MasterTopXRot)" />
  <parameter name="Rh1Mirror2MasterTopZLocationInLHCb" value="Rh1Mirror2BotCornerOfTopMirrLHCbZ - Rh1Mirror2MasterShiftInLocalY*sin(-1.0*Rh1Mirror2MasterTopXRot)+ Rh1Mirror2MasterShiftInLocalZ*cos(-1.0*Rh1Mirror2MasterTopXRot)" />

  <parameter name="Rh1Mirror2MasterBotXLocationInLHCb" value="0.0*mm" />
  <parameter name="Rh1Mirror2MasterBotYLocationInLHCb" value="Rh1Mirror2TopCornerOfBotMirrLHCbY + (-1.0* Rh1Mirror2MasterShiftInLocalY)*cos(-1.0*Rh1Mirror2MasterBotXRot) +  Rh1Mirror2MasterShiftInLocalZ*sin(-1.0*Rh1Mirror2MasterBotXRot)" />
  <parameter name="Rh1Mirror2MasterBotZLocationInLHCb" value="Rh1Mirror2TopCornerOfBotMirrLHCbZ - (-1.0*Rh1Mirror2MasterShiftInLocalY)*sin(-1.0*Rh1Mirror2MasterBotXRot) +  Rh1Mirror2MasterShiftInLocalZ*cos(-1.0*Rh1Mirror2MasterBotXRot)" />

  <parameter name="Rh1Mirror2MasterTopXLocation" value="Rh1Mirror2MasterTopXLocationInLHCb-Rich1MasterX-Rh1SubMasterX" />
  <parameter name="Rh1Mirror2MasterTopYLocation" value="Rh1Mirror2MasterTopYLocationInLHCb-Rich1MasterY-Rh1SubMasterY" />
  <parameter name="Rh1Mirror2MasterTopZLocation" value="Rh1Mirror2MasterTopZLocationInLHCb-Rich1MasterZ-Rh1SubMasterZ" />
  <parameter name="Rh1Mirror2MasterBotXLocation" value="Rh1Mirror2MasterBotXLocationInLHCb-Rich1MasterX-Rh1SubMasterX" />
  <parameter name="Rh1Mirror2MasterBotYLocation" value="Rh1Mirror2MasterBotYLocationInLHCb-Rich1MasterY-Rh1SubMasterY" />
  <parameter name="Rh1Mirror2MasterBotZLocation" value="Rh1Mirror2MasterBotZLocationInLHCb-Rich1MasterZ-Rh1SubMasterZ" />


<!-- end of Rich1Mirror2Master definition -->

 <!--  Now for Rich1 Mirror2 Support Panel. -->
 <!-- The following locations are extracted from EDMS document 860274 page 7.
     The sizes are approximate only -->

  <parameter name="Rh1Mirror2SupXSize" value="1200.0*mm"  />
  <parameter name="Rh1Mirror2SupYSize" value="600.0*mm"  />
  <parameter name="Rh1Mirror2SupZSize" value="25.0*mm"  />

<!-- The following two parameters for the support plate are not used but kept here for info. This
   gap gives the constraint on how much the Rh1Mirror2master can rotate
   without causing overlaps with the support plate. -->

  <parameter name="Rh1Mirror2SupGapFromMirror2" value="15.0*mm"  />
  <parameter name="Rh1Mirror2SupShiftInLocalY" value="-20.0*mm" />


<!-- The following parameters for the support plate are from Survey measurements.
     A correction applied in August 2008 to the Z location of the following Support plate
     since the mirror system moved by 2.8 mm upstream wrt the previous survey by DW et. al.
     So the Z location is moved up to 1220.1-2.8= 1217.3
   <parameter name="Rh1Mirror2SupZTopLocationInLHCb" value="1220.1*mm" />
   <parameter name="Rh1Mirror2SupZBotLocationInLHCb" value="1220.1*mm" />
   until Dec 2009
  <parameter name="Rh1Mirror2SupXLocationInLHCb" value="0.0*mm" />
  <parameter name="Rh1Mirror2SupYTopLocationInLHCb" value="689.6*mm" />
  <parameter name="Rh1Mirror2SupZTopLocationInLHCb" value="1217.3*mm" />
  <parameter name="Rh1Mirror2SupYBotLocationInLHCb" value="-688.7*mm" />
  <parameter name="Rh1Mirror2SupZBotLocationInLHCb" value="1217.3*mm" />
   In Dec 2009 since the survey of the bottom mirrors changed to downstream and down by the survey,
   a similar but slightly smaller shift made to the support plate at the bottom.
    Hence Z location now kept to the original survey value of 1220.1 at the bottom.
          Y location of plate moved down by 0.9 mm to -688.7-0.9= -689.6
  from Dec-10- 2009 onwards.
-->
  <parameter name="Rh1Mirror2SupXLocationInLHCb" value="0.0*mm" />
  <parameter name="Rh1Mirror2SupYTopLocationInLHCb" value="689.6*mm" />
  <parameter name="Rh1Mirror2SupZTopLocationInLHCb" value="1217.3*mm" />
  <parameter name="Rh1Mirror2SupYBotLocationInLHCb" value="-689.6*mm" />
  <parameter name="Rh1Mirror2SupZBotLocationInLHCb" value="1220.1*mm" />
  <parameter name="Rh1Mirror2SupXLocation" value="Rh1Mirror2SupXLocationInLHCb-Rich1MasterX-Rh1SubMasterX" />
  <parameter name="Rh1Mirror2SupYTopLocation" value="Rh1Mirror2SupYTopLocationInLHCb-Rich1MasterY-Rh1SubMasterY" />
  <parameter name="Rh1Mirror2SupZTopLocation" value="Rh1Mirror2SupZTopLocationInLHCb-Rich1MasterZ-Rh1SubMasterZ" />
  <parameter name="Rh1Mirror2SupYBotLocation" value="Rh1Mirror2SupYBotLocationInLHCb-Rich1MasterY-Rh1SubMasterY" />
  <parameter name="Rh1Mirror2SupZBotLocation" value="Rh1Mirror2SupZBotLocationInLHCb-Rich1MasterZ-Rh1SubMasterZ" />

  <parameter name="Rh1Mirror2SupTopXRot" value="Rh1Mirror2MasterTopXRot" />
  <parameter name="Rh1Mirror2SupBotXRot" value="Rh1Mirror2MasterBotXRot" />
  <parameter name="Rh1Mirror2SupTopYRot" value="Rh1Mirror2MasterTopYRot" />
  <parameter name="Rh1Mirror2SupBotYRot" value="Rh1Mirror2MasterBotYRot" />

<!--  Now start  the parameters for the actual mirrors  -->

  <parameter name="Rh1Mirror2InnerR00" value="778.3*m" />
  <parameter name="Rh1Mirror2InnerR01" value="1813.3*m" />
  <parameter name="Rh1Mirror2InnerR02" value="1564.9*m" />
  <parameter name="Rh1Mirror2InnerR03" value="1722.2*m" />
  <parameter name="Rh1Mirror2InnerR04" value="1564.9*m" />
  <parameter name="Rh1Mirror2InnerR05" value="712.7*m" />
  <parameter name="Rh1Mirror2InnerR06" value="632.6*m" />
  <parameter name="Rh1Mirror2InnerR07" value="1639.8*m" />
  <parameter name="Rh1Mirror2InnerR08" value="1185.1 *m" />
  <parameter name="Rh1Mirror2InnerR09" value="857.0*m" />
  <parameter name="Rh1Mirror2InnerR10" value="1722.2 *m" />
  <parameter name="Rh1Mirror2InnerR11" value="953.1*m" />
  <parameter name="Rh1Mirror2InnerR12" value="2654.1*m" />
  <parameter name="Rh1Mirror2InnerR13" value="1040.5*m" />
  <parameter name="Rh1Mirror2InnerR14" value="1433.8*m" />
  <parameter name="Rh1Mirror2InnerR15" value="632.6*m" />



 <parameter name="Rh1Mirror2SingleAngularXExtent00" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR00)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent00" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR00" />
 <parameter name="Rh1Mirror2OuterR00" value="Rh1Mirror2InnerR00+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta00" value="asin(Rh1Mirror2SingleAngularXExtent00)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi00"   value="asin(Rh1Mirror2SingleAngularYExtent00)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart00" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta00)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart00"  value="-0.5*Rh1Mirror2SingleDeltaPhi00" />

  <!-- The following are the gaps at the inner reflecting surfaces of the mirrors calculated from
    the net gaps at the outer surfaces of the mirrors . Same thing repeated for other mirrors-->


 <parameter name="Rh1Mirror2XHalfTotGap00" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent00)" />
 <parameter name="Rh1Mirror2YHalfTotGap00" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent00)" />

 <parameter name="Rh1Mirror2ZCCShift00"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR00*cos(0.5*Rh1Mirror2SingleAngularYExtent00)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent01" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR01)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent01" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR01" />
 <parameter name="Rh1Mirror2OuterR01" value="Rh1Mirror2InnerR01+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta01" value="asin(Rh1Mirror2SingleAngularXExtent01)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi01"   value="asin(Rh1Mirror2SingleAngularYExtent01)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart01" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta01)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart01"  value="-0.5*Rh1Mirror2SingleDeltaPhi01" />

 <parameter name="Rh1Mirror2XHalfTotGap01" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent01)" />
 <parameter name="Rh1Mirror2YHalfTotGap01" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent01)" />

 <parameter name="Rh1Mirror2ZCCShift01"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR01*cos(0.5*Rh1Mirror2SingleAngularYExtent01)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent02" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR02)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent02" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR02" />
 <parameter name="Rh1Mirror2OuterR02" value="Rh1Mirror2InnerR02+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta02" value="asin(Rh1Mirror2SingleAngularXExtent02)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi02"   value="asin(Rh1Mirror2SingleAngularYExtent02)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart02" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta02)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart02"  value="-0.5*Rh1Mirror2SingleDeltaPhi02" />

 <parameter name="Rh1Mirror2XHalfTotGap02" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent02)" />
 <parameter name="Rh1Mirror2YHalfTotGap02" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent02)" />

 <parameter name="Rh1Mirror2ZCCShift02"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR02*cos(0.5*Rh1Mirror2SingleAngularYExtent02)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent03" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR03)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent03" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR03" />
 <parameter name="Rh1Mirror2OuterR03" value="Rh1Mirror2InnerR03+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta03" value="asin(Rh1Mirror2SingleAngularXExtent03)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi03"   value="asin(Rh1Mirror2SingleAngularYExtent03)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart03" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta03)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart03"  value="-0.5*Rh1Mirror2SingleDeltaPhi03" />

 <parameter name="Rh1Mirror2XHalfTotGap03" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent03)" />
 <parameter name="Rh1Mirror2YHalfTotGap03" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent03)" />

 <parameter name="Rh1Mirror2ZCCShift03"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR03*cos(0.5*Rh1Mirror2SingleAngularYExtent03)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent04" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR04)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent04" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR04" />
 <parameter name="Rh1Mirror2OuterR04" value="Rh1Mirror2InnerR04+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta04" value="asin(Rh1Mirror2SingleAngularXExtent04)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi04"   value="asin(Rh1Mirror2SingleAngularYExtent04)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart04" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta04)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart04"  value="-0.5*Rh1Mirror2SingleDeltaPhi04" />

 <parameter name="Rh1Mirror2XHalfTotGap04" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent04)" />
 <parameter name="Rh1Mirror2YHalfTotGap04" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent04)" />

 <parameter name="Rh1Mirror2ZCCShift04"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR04*cos(0.5*Rh1Mirror2SingleAngularYExtent04)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent05" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR05)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent05" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR05" />
 <parameter name="Rh1Mirror2OuterR05" value="Rh1Mirror2InnerR05+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta05" value="asin(Rh1Mirror2SingleAngularXExtent05)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi05"   value="asin(Rh1Mirror2SingleAngularYExtent05)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart05" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta05)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart05"  value="-0.5*Rh1Mirror2SingleDeltaPhi05" />

 <parameter name="Rh1Mirror2XHalfTotGap05" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent05)" />
 <parameter name="Rh1Mirror2YHalfTotGap05" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent05)" />

 <parameter name="Rh1Mirror2ZCCShift05"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR05*cos(0.5*Rh1Mirror2SingleAngularYExtent05)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent06" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR06)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent06" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR06" />
 <parameter name="Rh1Mirror2OuterR06" value="Rh1Mirror2InnerR06+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta06" value="asin(Rh1Mirror2SingleAngularXExtent06)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi06"   value="asin(Rh1Mirror2SingleAngularYExtent06)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart06" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta06)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart06"  value="-0.5*Rh1Mirror2SingleDeltaPhi06" />

 <parameter name="Rh1Mirror2XHalfTotGap06" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent06)" />
 <parameter name="Rh1Mirror2YHalfTotGap06" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent06)" />

 <parameter name="Rh1Mirror2ZCCShift06"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR06*cos(0.5*Rh1Mirror2SingleAngularYExtent06)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent07" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR07)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent07" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR07" />
 <parameter name="Rh1Mirror2OuterR07" value="Rh1Mirror2InnerR07+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta07" value="asin(Rh1Mirror2SingleAngularXExtent07)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi07"   value="asin(Rh1Mirror2SingleAngularYExtent07)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart07" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta07)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart07"  value="-0.5*Rh1Mirror2SingleDeltaPhi07" />

 <parameter name="Rh1Mirror2XHalfTotGap07" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent07)" />
 <parameter name="Rh1Mirror2YHalfTotGap07" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent07)" />

 <parameter name="Rh1Mirror2ZCCShift07"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR07*cos(0.5*Rh1Mirror2SingleAngularYExtent07)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent08" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR08)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent08" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR08" />
 <parameter name="Rh1Mirror2OuterR08" value="Rh1Mirror2InnerR08+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta08" value="asin(Rh1Mirror2SingleAngularXExtent08)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi08"   value="asin(Rh1Mirror2SingleAngularYExtent08)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart08" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta08)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart08"  value="-0.5*Rh1Mirror2SingleDeltaPhi08" />

 <parameter name="Rh1Mirror2XHalfTotGap08" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent08)" />
 <parameter name="Rh1Mirror2YHalfTotGap08" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent08)" />

 <parameter name="Rh1Mirror2ZCCShift08"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR08*cos(0.5*Rh1Mirror2SingleAngularYExtent08)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent09" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR09)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent09" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR09" />
 <parameter name="Rh1Mirror2OuterR09" value="Rh1Mirror2InnerR09+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta09" value="asin(Rh1Mirror2SingleAngularXExtent09)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi09"   value="asin(Rh1Mirror2SingleAngularYExtent09)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart09" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta09)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart09"  value="-0.5*Rh1Mirror2SingleDeltaPhi09" />

 <parameter name="Rh1Mirror2XHalfTotGap09" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent09)" />
 <parameter name="Rh1Mirror2YHalfTotGap09" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent09)" />

 <parameter name="Rh1Mirror2ZCCShift09"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR09*cos(0.5*Rh1Mirror2SingleAngularYExtent09)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent10" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR10)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent10" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR10" />
 <parameter name="Rh1Mirror2OuterR10" value="Rh1Mirror2InnerR10+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta10" value="asin(Rh1Mirror2SingleAngularXExtent10)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi10"   value="asin(Rh1Mirror2SingleAngularYExtent10)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart10" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta10)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart10"  value="-0.5*Rh1Mirror2SingleDeltaPhi10" />

 <parameter name="Rh1Mirror2XHalfTotGap10" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent10)" />
 <parameter name="Rh1Mirror2YHalfTotGap10" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent10)" />

 <parameter name="Rh1Mirror2ZCCShift10"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR10*cos(0.5*Rh1Mirror2SingleAngularYExtent10)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent11" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR11)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent11" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR11" />
 <parameter name="Rh1Mirror2OuterR11" value="Rh1Mirror2InnerR11+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta11" value="asin(Rh1Mirror2SingleAngularXExtent11)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi11"   value="asin(Rh1Mirror2SingleAngularYExtent11)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart11" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta11)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart11"  value="-0.5*Rh1Mirror2SingleDeltaPhi11" />

 <parameter name="Rh1Mirror2XHalfTotGap11" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent11)" />
 <parameter name="Rh1Mirror2YHalfTotGap11" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent11)" />

 <parameter name="Rh1Mirror2ZCCShift11"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR11*cos(0.5*Rh1Mirror2SingleAngularYExtent11)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent12" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR12)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent12" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR12" />
 <parameter name="Rh1Mirror2OuterR12" value="Rh1Mirror2InnerR12+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta12" value="asin(Rh1Mirror2SingleAngularXExtent12)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi12"   value="asin(Rh1Mirror2SingleAngularYExtent12)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart12" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta12)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart12"  value="-0.5*Rh1Mirror2SingleDeltaPhi12" />

 <parameter name="Rh1Mirror2XHalfTotGap12" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent12)" />
 <parameter name="Rh1Mirror2YHalfTotGap12" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent12)" />

 <parameter name="Rh1Mirror2ZCCShift12"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR12*cos(0.5*Rh1Mirror2SingleAngularYExtent12)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent13" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR13)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent13" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR13" />
 <parameter name="Rh1Mirror2OuterR13" value="Rh1Mirror2InnerR13+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta13" value="asin(Rh1Mirror2SingleAngularXExtent13)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi13"   value="asin(Rh1Mirror2SingleAngularYExtent13)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart13" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta13)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart13"  value="-0.5*Rh1Mirror2SingleDeltaPhi13" />

 <parameter name="Rh1Mirror2XHalfTotGap13" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent13)" />
 <parameter name="Rh1Mirror2YHalfTotGap13" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent13)" />

 <parameter name="Rh1Mirror2ZCCShift13"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR13*cos(0.5*Rh1Mirror2SingleAngularYExtent13)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent14" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR14)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent14" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR14" />
 <parameter name="Rh1Mirror2OuterR14" value="Rh1Mirror2InnerR14+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta14" value="asin(Rh1Mirror2SingleAngularXExtent14)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi14"   value="asin(Rh1Mirror2SingleAngularYExtent14)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart14" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta14)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart14"  value="-0.5*Rh1Mirror2SingleDeltaPhi14" />

 <parameter name="Rh1Mirror2XHalfTotGap14" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent14)" />
 <parameter name="Rh1Mirror2YHalfTotGap14" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent14)" />

 <parameter name="Rh1Mirror2ZCCShift14"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR14*cos(0.5*Rh1Mirror2SingleAngularYExtent14)" />



 <parameter name="Rh1Mirror2SingleAngularXExtent15" value="(Rh1Mirror2SingleXSize/Rh1Mirror2InnerR15)" />
 <parameter name="Rh1Mirror2SingleAngularYExtent15" value="Rh1Mirror2SingleYSize/Rh1Mirror2InnerR15" />
 <parameter name="Rh1Mirror2OuterR15" value="Rh1Mirror2InnerR15+Rh1Mirror2ZSize" />

 <parameter name="Rh1Mirror2SingleDeltaTheta15" value="asin(Rh1Mirror2SingleAngularXExtent15)"  />
 <parameter name="Rh1Mirror2SingleDeltaPhi15"   value="asin(Rh1Mirror2SingleAngularYExtent15)"  />
 <parameter name="Rh1Mirror2SingleThetaSegmentStart15" value="(pi/2.0)*rad-(0.5*Rh1Mirror2SingleDeltaTheta15)" />
 <parameter name="Rh1Mirror2SinglePhiSegmentStart15"  value="-0.5*Rh1Mirror2SingleDeltaPhi15" />

 <parameter name="Rh1Mirror2XHalfTotGap15" value="0.5*(Rh1Mirror2XNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularXExtent15)" />
 <parameter name="Rh1Mirror2YHalfTotGap15" value="0.5*(Rh1Mirror2YNetGap+Rh1Mirror2ZSize*Rh1Mirror2SingleAngularYExtent15)" />

 <parameter name="Rh1Mirror2ZCCShift15"  value="-Rh1Mirror2MasterShiftInLocalZ+Rh1Mirror2InnerR15*cos(0.5*Rh1Mirror2SingleAngularYExtent15)" />




 <!-- now for the rotations of the mirror segments -->
  <parameter name="Rh1Mirror2ThetaShift" value="(pi/2)*rad" />
  <parameter name="Rh1Mirror2YRot" value= "Rh1Mirror2ThetaShift" />
 <!-- Now for placing the mirror segment COCs in the Rich1Mirror2Master System. Z shift already done above-->
 <parameter name="Rh1Mirror2XCCShift01"  value="Rh1Mirror2XHalfTotGap01+0.5*Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift03"  value="Rh1Mirror2XHalfTotGap03+0.5*Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift09"  value="Rh1Mirror2XHalfTotGap09+0.5*Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift11"  value="Rh1Mirror2XHalfTotGap11+0.5*Rh1Mirror2SingleXSize"  />

 <parameter name="Rh1Mirror2XCCShift04"  value="-Rh1Mirror2XHalfTotGap04-0.5*Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift06"  value="-Rh1Mirror2XHalfTotGap06-0.5*Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift12"  value="-Rh1Mirror2XHalfTotGap12-0.5*Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift14"  value="-Rh1Mirror2XHalfTotGap14-0.5*Rh1Mirror2SingleXSize"  />

 <parameter name="Rh1Mirror2XCCShift00"  value="Rh1Mirror2XCCShift01+Rh1Mirror2XHalfTotGap01+ Rh1Mirror2XHalfTotGap00+ Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift02"  value="Rh1Mirror2XCCShift03+Rh1Mirror2XHalfTotGap03+ Rh1Mirror2XHalfTotGap02+ Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift08"  value="Rh1Mirror2XCCShift09+Rh1Mirror2XHalfTotGap09+ Rh1Mirror2XHalfTotGap08+ Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift10"  value="Rh1Mirror2XCCShift11+Rh1Mirror2XHalfTotGap11+ Rh1Mirror2XHalfTotGap10+ Rh1Mirror2SingleXSize"  />

 <parameter name="Rh1Mirror2XCCShift05"  value="Rh1Mirror2XCCShift04-Rh1Mirror2XHalfTotGap04- Rh1Mirror2XHalfTotGap05- Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift07"  value="Rh1Mirror2XCCShift06-Rh1Mirror2XHalfTotGap06- Rh1Mirror2XHalfTotGap07- Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift13"  value="Rh1Mirror2XCCShift12-Rh1Mirror2XHalfTotGap12- Rh1Mirror2XHalfTotGap13- Rh1Mirror2SingleXSize"  />
 <parameter name="Rh1Mirror2XCCShift15"  value="Rh1Mirror2XCCShift14-Rh1Mirror2XHalfTotGap14- Rh1Mirror2XHalfTotGap15- Rh1Mirror2SingleXSize"  />


 <parameter name="Rh1Mirror2YCCShift02"  value="-Rh1Mirror2MasterShiftInLocalY+0.5*Rh1Mirror2SingleYSize" />
 <parameter name="Rh1Mirror2YCCShift03"  value="Rh1Mirror2YCCShift02" />
 <parameter name="Rh1Mirror2YCCShift06"  value="Rh1Mirror2YCCShift02" />
 <parameter name="Rh1Mirror2YCCShift07"  value="Rh1Mirror2YCCShift02" />
 <parameter name="Rh1Mirror2YCCShift00"  value="Rh1Mirror2YCCShift02+Rh1Mirror2YHalfTotGap02+Rh1Mirror2YHalfTotGap00+Rh1Mirror2SingleYSize" />
 <parameter name="Rh1Mirror2YCCShift01"  value="Rh1Mirror2YCCShift03+Rh1Mirror2YHalfTotGap03+Rh1Mirror2YHalfTotGap01+Rh1Mirror2SingleYSize" />
 <parameter name="Rh1Mirror2YCCShift04"  value="Rh1Mirror2YCCShift06+Rh1Mirror2YHalfTotGap06+Rh1Mirror2YHalfTotGap04+Rh1Mirror2SingleYSize" />
 <parameter name="Rh1Mirror2YCCShift05"  value="Rh1Mirror2YCCShift07+Rh1Mirror2YHalfTotGap07+Rh1Mirror2YHalfTotGap05+Rh1Mirror2SingleYSize" />

 <parameter name="Rh1Mirror2YCCShift08"  value="Rh1Mirror2MasterShiftInLocalY-0.5*Rh1Mirror2SingleYSize" />
 <parameter name="Rh1Mirror2YCCShift09"  value="Rh1Mirror2YCCShift08" />
 <parameter name="Rh1Mirror2YCCShift12"  value="Rh1Mirror2YCCShift08" />
 <parameter name="Rh1Mirror2YCCShift13"  value="Rh1Mirror2YCCShift08" />
 <parameter name="Rh1Mirror2YCCShift10"  value="Rh1Mirror2YCCShift08-Rh1Mirror2YHalfTotGap08-Rh1Mirror2YHalfTotGap10-Rh1Mirror2SingleYSize" />
 <parameter name="Rh1Mirror2YCCShift11"  value="Rh1Mirror2YCCShift09-Rh1Mirror2YHalfTotGap09-Rh1Mirror2YHalfTotGap11-Rh1Mirror2SingleYSize" />
 <parameter name="Rh1Mirror2YCCShift14"  value="Rh1Mirror2YCCShift12-Rh1Mirror2YHalfTotGap12-Rh1Mirror2YHalfTotGap14-Rh1Mirror2SingleYSize" />
 <parameter name="Rh1Mirror2YCCShift15"  value="Rh1Mirror2YCCShift13-Rh1Mirror2YHalfTotGap13-Rh1Mirror2YHalfTotGap15-Rh1Mirror2SingleYSize" />


 <!--        End of derived parameters for the Rich1 Mirror       -->
 <!--    Now start the parameters derived for the Brunel application  . The following 6 lines can be removed
        in the future. For now kept for backward compatibility   -->
    <parameter name = "Rh1Mirror2NorSlope" value="tan(Rh1Mirror2VertTilt)" />
    <parameter name = "Rh1Mirror2CosSum"  value = "pow((1.0+Rh1Mirror2NorSlope*Rh1Mirror2NorSlope),0.5)" />
    <parameter name = "Rh1Mirror2CosX" value = "0.0" />
    <parameter name = "Rh1Mirror2CosY" value = "Rh1Mirror2NorSlope/Rh1Mirror2CosSum" />
    <parameter name = "Rh1Mirror2CosZ" value = "1.0/Rh1Mirror2CosSum" />
    <parameter name = "Rh1Mirror2DistFromNominalOrigin"
      value="Rh1Mirror2BotCornerLHCbZ*cos(Rh1Mirror2VertTilt)+Rh1Mirror2BotCornerLHCbY*sin(Rh1Mirror2VertTilt)" />



    <parameter name = "Rh1Mirror2NorVertSlopeTop" value="tan(Rh1Mirror2MasterTopXRot)" />
    <parameter name = "Rh1Mirror2NorHorizSlopeTop" value="tan(Rh1Mirror2MasterTopYRot)" />
    <parameter name = "Rh1Mirror2CosSumTop"  value = "pow((1.0+Rh1Mirror2NorVertSlopeTop*Rh1Mirror2NorVertSlopeTop+ Rh1Mirror2NorHorizSlopeTop*Rh1Mirror2NorHorizSlopeTop ),0.5)" />
    <parameter name = "Rh1Mirror2TopCosX" value = "Rh1Mirror2NorHorizSlopeTop/Rh1Mirror2CosSumTop" />
    <parameter name = "Rh1Mirror2TopCosY" value = "Rh1Mirror2NorVertSlopeTop/Rh1Mirror2CosSumTop" />
    <parameter name = "Rh1Mirror2TopCosZ" value = "1.0/Rh1Mirror2CosSumTop" />
    <parameter name = "Rh1Mirror2DistFromNominalOriginTop"
      value="Rh1Mirror2BotCornerOfTopMirrLHCbZ*cos(Rh1Mirror2TopVertTilt)+Rh1Mirror2BotCornerOfTopMirrLHCbY*sin(Rh1Mirror2TopVertTilt)" />


    <parameter name = "Rh1Mirror2NorVertSlopeBot" value="tan(Rh1Mirror2MasterBotXRot)" />
    <parameter name = "Rh1Mirror2NorHorizSlopeBot" value="tan(Rh1Mirror2MasterBotYRot)" />
    <parameter name = "Rh1Mirror2CosSumBot"  value = "pow((1.0+Rh1Mirror2NorVertSlopeBot*Rh1Mirror2NorVertSlopeBot+ Rh1Mirror2NorHorizSlopeBot*Rh1Mirror2NorHorizSlopeBot),0.5)" />
    <parameter name = "Rh1Mirror2BotCosX" value = "Rh1Mirror2NorHorizSlopeBot/Rh1Mirror2CosSumBot" />
    <parameter name = "Rh1Mirror2BotCosY" value = "Rh1Mirror2NorVertSlopeBot/Rh1Mirror2CosSumBot" />
    <parameter name = "Rh1Mirror2BotCosZ" value = "1.0/Rh1Mirror2CosSumBot" />
    <parameter name = "Rh1Mirror2DistFromNominalOriginBot"
      value="Rh1Mirror2TopCornerOfBotMirrLHCbZ*cos(Rh1Mirror2BotVertTilt)+Rh1Mirror2TopCornerOfBotMirrLHCbY*sin(Rh1Mirror2BotVertTilt)" />
<!--        End   Rich1MirrorGeomParameters    -->










